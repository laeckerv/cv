
export function toDateString(date: string) {
  const month = parseInt(date.substring(3,5));
  const year = parseInt(date.substring(6));

  return `${months[month]} ${year}`;
}

const months = {
  1: "Jan",
  2: "Feb",
  3: "Mar",
  4: "Apr",
  5: "May",
  6: "Jun",
  7: "Jul",
  8: "Aug",
  9: "Sep",
  10: "Oct",
  11: "Nov",
  12: "Dec"
};
