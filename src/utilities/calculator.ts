import type { Cv } from "../models/cv";

export function getWeightedLanguages(cv: Cv) {
  const list = {};

  cv.work.filter(w => w.languages).forEach(w => w.languages.forEach(l => {
    const key = l.toLowerCase();
    list[key] = list[key] === undefined ? 1 : list[key] + 1;
  }))

  return list;
}

export function getWeightedTechnologies(cv: Cv) {
  const list = {};

  cv.work.filter(w => w.technologies).forEach(w => w.technologies.forEach(t => {
    const key = t.toLowerCase();
    list[key] = list[key] === undefined ? 1 : list[key] + 1;
  }))

  return list;
}


export function addProjectDescriptionWeighs(cv:Cv, weightedList: Record<string,number>) {
  const list = {...weightedList};

  cv.work.filter(w => w.projects).forEach(w => w.projects.forEach(p => {
    Object.keys(weightedList).forEach(k => {
      const regexp = new RegExp(` ${k.replace(/\+/g, "\\+")}[\. ,\"]+`, "i");
      if(regexp.test(p)) {
        list[k] = list[k] + 1;
      }
    })
  }))

  return list;

}
