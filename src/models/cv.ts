  export interface Birth {
      place: string;
      date: string;
      nationality: string;
  }

  export interface Address {
      street: string;
      city: string;
  }

  export interface Contact {
      name: string;
      surname: string;
      birth: Birth;
      maritalStatus: string;
      permit: string;
      address: Address;
      phone: string;
      mail: string;
      linkedin: string;
  }

  export interface Duration {
      start: string;
      end: string;
  }

  export interface Thesis {
      subject: string,
      grade: string,
  }

  export interface Education {
      title: string;
      school: string;
      duration: Duration;
      gpa?: string;
      subject?: string;
      thesis: Thesis;
  }

  export interface Company {
      name: string;
      description?: string;
  }

  export interface Work {
      title: string;
      position: string;
      company: Company;
      duration: Duration;
      languages?: string[];
      technologies?: string[];
      ides?: string[];
      technology?: string[];
      projects?: string[];
  }

  export interface Social {
      title: string;
      position: string;
      company: string;
      duration: Duration;
  }

  export interface Language {
      title: string;
      level: string;
      rating: number;
  }

  export interface Cv {
      title: string;
      contact: Contact;
      summary: string;
      education: Education[];
      work: Work[];
      social: Social[];
      language: Language[];
  }

