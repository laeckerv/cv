Generalist.  For the successful outcome of a project I pick the appropriate tools and suitable technologies. In order to achieve that I constantly follow new trends and explore new technologies. I enjoy the process of understanding new approaches to existing problems.
When I find an interesting subject I take a time-boxed approach to dive deeper. If the result seems to have potential for our work I like to share my gained knowledge with my peers.
To improve my work I read books and blogs about design patterns, software architecture and programming concepts (just to name a few).


- love to code
- love to find solutions for difficult problems
- love challenges - there is always some way
- love to use new technologies and approaches supported by well known principles and patterns
- love to work in a great team
- love to ride the hype cycle and figure out what can be the next big thing
- love to find things and ways that make our lives as a developer/peer/team/company easier
- love to share knowledge with others
- love to gain knowledge from others
- love to interact with others


Generalist. Craftsman. Love for code. Team player. Driven by knowledge and progress. Fail fast and learn.

